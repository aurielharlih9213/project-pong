﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
	// Tombol Gerak
	public KeyCode upButton = KeyCode.W;
	public KeyCode downButton = KeyCode.S;

	// Kecepatan Gerak
	public float speed = 10.0f;
	// Batas Atas dan Bawah
	public float yBoundary = 9.0f;
    // Rigidbody Raket
    private Rigidbody2D rigidBody2D;
    // Skor Pemain
    private int score;

    // Power up
    public int powerUp;
    private bool isPowerUp = false;
    private bool startTimer = false;
    float currentTime = 0f;
    float startingTime = 3.0f;
    public int chancePowerUp = 3;
    Vector3 changeScale = new Vector3(0, 1.0f, 0);
    
    // Tombol Power Up
    public KeyCode powerButton = KeyCode.D;

    // Titik tumbukan terakhir dengan bola
    private ContactPoint2D lastContactPoint;
    public ContactPoint2D LastContactPoint {
    	get {return lastContactPoint;}
    }

    // Ketika terjadi tumbukan dengan bola, rekam titik kontaknya
    void OnCollisionEnter2D(Collision2D collision) {
    	if (collision.gameObject.name.Equals("Ball")) {
    		lastContactPoint = collision.GetContact(0);
    	}
    }

    void Start()
    {
        rigidBody2D = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Ambil Kecepatan Raket Sekarang
        Vector2 velocity = rigidBody2D.velocity;

        // Jika pemain menekan tombol keatas, beri kecepatan positif komponen y
        if (Input.GetKey(upButton)) {
        	velocity.y = speed;
        }
        // Jika pemain menekan tombol kebawah, beri kecepatan negatif komponen y
        else if (Input.GetKey(downButton)) {
        	velocity.y = -speed;
        }
        // Jika pemain tidak menekan apapun
        else {
        	velocity.y = 0.0f;
        }

        // Power Up
        if (Input.GetKeyUp(powerButton) && isPowerUp == false && chancePowerUp > 0) {
        	isPowerUp = true;
    		this.transform.localScale += changeScale;
    		chancePowerUp -= 1;
        	currentTime = startingTime;
        	startTimer = true;
        }

        if (startTimer == true) {
        	currentTime -= 1 * Time.deltaTime;
        	if (currentTime <= 0) {
        		isPowerUp = false;
    			this.transform.localScale -= changeScale;
        		startTimer = false;
        	}
        }
		

        // Masukkan kembali kecepatan ke rigidbody
        rigidBody2D.velocity = velocity;
    
        // Dapatkan posisi raket
        Vector3 position = transform.position;

        // Jika posisi raket melewati batas atas
        if (position.y > yBoundary) {
        	position.y = yBoundary;
        }
        // Jika Posisi raket melewati batas bawah
        else if (position.y < - yBoundary) {
        	position.y = -yBoundary;
        }

        // Masukkan kembali posisi
        transform.position = position;
    }

    public void IncrementScore() {
    	score++;
    }

    public void ResetScore() {
    	score = 0;
    	chancePowerUp = 3;
    }

    public int Score {
    	get { return score; }
    }
}
