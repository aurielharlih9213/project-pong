﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SideWall : MonoBehaviour
{
	// Pemain yang akan bertambah skor
	public PlayerController player;

	[SerializeField]
	private GameManager gameManager;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    // Dipanggil saat bola bersentuhan denngan dinding
    void OnTriggerEnter2D(Collider2D anotherCollider) {
    	if (anotherCollider.name == "Ball") {
    		// Tambah skor pemain
    		player.IncrementScore();
    	
    		// Jika skor pemain belum capai maksimal
    		if (player.Score < gameManager.maxScore) {
    			// Restart game
    			anotherCollider.gameObject.SendMessage("RestartGame", 2.0f, SendMessageOptions.RequireReceiver);
    		}
    	}
    }
}
