﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trajectory : MonoBehaviour
{
	// Collider, rigidbody, skrip bola
	Rigidbody2D ballRigidbody;
	CircleCollider2D ballCollider;
	public BallControl ball;

	// Bola bayangan yang ditampilkan di titik tumbukan
	public GameObject ballAtCollision;

    // Start is called before the first frame update
    void Start()
    {
        ballRigidbody = ball.GetComponent<Rigidbody2D>();
        ballCollider = ball.GetComponent<CircleCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
    	// Status pantulan, yang aktif saat hanya bertumbukan dengan objek tertentu
        bool drawBallAtCollision = false;

        // Titik tumbukan yang digeser, untuk menggambar ballAtCollision
        Vector2 offsetHitPoint = new Vector2();

        // Tentukan titik tumbukan dengan deteksi pergerakan lingkaran
        RaycastHit2D[] circleCastHit2DArray = Physics2D.CircleCastAll(ballRigidbody.position, ballCollider.radius, ballRigidbody.velocity.normalized);

        // Untuk setiap tumbukan
        foreach (RaycastHit2D circleCastHit2D in circleCastHit2DArray) {
        	// jika terjadi tumbukan, dan tidak dengan bola
        	// (karena garis lintasan digambar dari titik tengah bola)
        	if (circleCastHit2D.collider != null && circleCastHit2D.collider.GetComponent<BallControl>() == null) {
        		// Lintasan digambar dari titik tengah bola sekarang sampai titik bola tumbukan

        		// Tentukan titik tumbukan
        		Vector2 hitPoint = circleCastHit2D.point;

        		// Tentukan normal di titik tumbukan
        		Vector2 hitNormal = circleCastHit2D.normal;

        		// Titik tengah bola saat bertumbukan
        		offsetHitPoint = hitPoint + hitNormal * ballCollider.radius;

        		// Gambar garis lintasan dari bola sekarang sampai ke titik tumbukan
        		DottedLine.DottedLine.Instance.DrawDottedLine(ball.transform.position, offsetHitPoint);

        		// Kalau bukan sideWall, gambar pantulannya
        		if (circleCastHit2D.collider.GetComponent<SideWall>() == null) {
        			// Hitung vektor datang
        			Vector2 inVector = (offsetHitPoint - ball.TrajectoryOrigin).normalized;

        			// Hitung vektor keluar
        			Vector2 outVector = Vector2.Reflect(inVector, hitNormal);

        			// Hitung Dot product dari outvector dan hitnormal. Agar garis lintasan ketika terjadi tumbukan tidak digambar
        			float outDot = Vector2.Dot(outVector, hitNormal);
        			if (outDot > -1.0f && outDot < 1.0f) {
        				// Gambar lintasan pantulan
        				DottedLine.DottedLine.Instance.DrawDottedLine(offsetHitPoint
        					, offsetHitPoint + outVector * 10.0f);

        				// Menggambar bola bayangan di prediksi titik tumbukan
        				drawBallAtCollision = true;
        			}
        		}
        		break;
        	}
        	if (drawBallAtCollision) {
        		// Gambar bola bayangan di titik tumbukan
        		ballAtCollision.transform.position = offsetHitPoint;
        		ballAtCollision.SetActive(true);
        	} else {
        		// Sembunyikan bola bayangan
        		ballAtCollision.SetActive(false);
        	}
        }
    }
}
